<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EtudiantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('etudiant')->insert(
            [
                'nom' => 'TCHANHOUIN',
                'prenom' => 'Amède Angel Aulerien',
                'sexe' => 'M',
                'date_naissance' => '2020-10-30',
                'email' => 'aulerien80@gmail.com'
            ]
        );
    }
}
