<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etudiant extends Model
{


    protected $table = "etudiant";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nom', 'prenom', 'sexe', 'date_naissance', 'email'
    ];


    public static $rules = [
        'nom' => 'required|string',
        'prenom' => 'required|string',
        'sexe' => 'required|string:1',
        'date_naissance' => 'required'
    ];

    /**
     * Get messages validator
     * @var array
     */
    public static $validatorMessages = [
        'nom.required' => "Le nom est obligatoire",
        'nom.string' => "Le nom doit être une chaîne de caratère",
        'prenom.required' => "Le prénom est obligatoire",
        'prenom.string' => "Le prénom doit être une chaîne de caratère",
        'sexe.required' => "Le sexe est obligatoire",
        'sexe.string:1' => "Le sexe doit être sur un seul caractère",
        'date_naissance' => "La date de naissance est obligatoire",
    ];
}
