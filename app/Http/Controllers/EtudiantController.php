<?php

namespace App\Http\Controllers;

use App\Models\Etudiant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class EtudiantController extends Controller
{

    /**
     * Afficher la liste des étudiants
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list() {
        $etudiants = Etudiant::query()->orderBy('nom')->get();
        return view('etudiants.list', compact('etudiants'));
    }

    /**
     * Afficher la page de modification d'un étudiant
     * @param Request $request
     * @param $etudiantId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function modification(Request $request, $etudiantId) {
        $etudiant = Etudiant::query()->where('id', '=', $etudiantId)->first();
        if($etudiant == null) {
            abort(404);
        }
        return view('etudiants.save', compact('etudiant'));
    }

    /**
     * Afficher la page de création d'un étudiant
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function creation(Request $request) {
        return view('etudiants.save');
    }

    /**
     * Enregistrer ou modifier un étudiant
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request){
        $validator = Validator::make($request->all(), Etudiant::$rules, Etudiant::$validatorMessages);
       // $this->validate($request->all(), Etudiant::$rules, Etudiant::$validatorMessages);
        $validator->validate();
        $etudiantId = $request->input('etudiant_id');
        $messageSuccess = "";
        if($etudiantId == null) { // création
            $etudiant = new Etudiant();
          //  Etudiant::query()->create($request->all());
            $messageSuccess = "Nouvel étudiant enregistré avec succès";
        } else { // modification
            $etudiant = Etudiant::query()->where('id', '=', $etudiantId)->first();
            if($etudiant == null){
                abort(404);
            }
            $messageSuccess = "Information de l'étudiant mises à jour avec succès";
        }
        $etudiant->nom = $request->input('nom');
        $etudiant->prenom = $request->input('prenom');
        $etudiant->sexe = $request->input('sexe');
        $etudiant->date_naissance = $request->input('date_naissance');
        $etudiant->email = $request->input('email');
        $etudiant->save();
        Session::flash('success', $messageSuccess);
        //return back
        return redirect()->back();
    }

    /**
     * Supprimer un étudiant
     * @param Request $request
     * @param $etudiantId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $request, $etudiantId) {
        $etudiant = Etudiant::query()->where('id', '=', $etudiantId)->first();
        if($etudiant == null){
            abort(404);
        }
        $etudiant->delete();
        Session::flash('success', "Etudiant supprimé avec succès");
        return redirect()->back();
    }
}
