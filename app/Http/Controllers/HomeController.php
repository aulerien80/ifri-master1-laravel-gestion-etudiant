<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index() {
        // return the welcome view
        return view('welcome');
    }

    public function accueil(){
        // return the accueil view
        return view('accueil');
    }
}
