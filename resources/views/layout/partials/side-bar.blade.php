<div class="app-sidebar__inner">
    <ul class="vertical-nav-menu">
        <li class="app-sidebar__heading">Dashboards</li>
        <li>
            <a href="{{route('accueil.custom')}}" class="mm-active">
                <i class="metismenu-icon pe-7s-rocket"></i>
                Accueil
            </a>
        </li>
        <li class="app-sidebar__heading">MENUS</li>
        <!-- etudiant -->
        <li>
            <a href="#">
                <i class="metismenu-icon pe-7s-diamond"></i>
                Etudiants
                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li>
                    <a href="{{url('etudiants/liste')}}">
                        <i class="metismenu-icon"></i>
                        Liste
                    </a>
                </li>
                <li>
                    <a href="{{route('etudiant.creation')}}">
                        <i class="metismenu-icon"></i>
                        Création
                    </a>
                </li>
            </ul>
        </li>

        <!-- matieres -->
        <li>
            <a href="#">
                <i class="metismenu-icon pe-7s-diamond"></i>
                Matières
                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li>
                    <a href="elements-buttons-standard.html">
                        <i class="metismenu-icon"></i>
                        Liste
                    </a>
                </li>
                <li>
                    <a href="elements-dropdowns.html">
                        <i class="metismenu-icon"></i>
                        Création
                    </a>
                </li>
            </ul>
        </li>

        <!-- notes -->
        <li>
            <a href="#">
                <i class="metismenu-icon pe-7s-diamond"></i>
                Notes
                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li>
                    <a href="elements-buttons-standard.html">
                        <i class="metismenu-icon"></i>
                        Liste
                    </a>
                </li>
                <li>
                    <a href="elements-dropdowns.html">
                        <i class="metismenu-icon"></i>
                        Création
                    </a>
                </li>
            </ul>
        </li>
        <li class="app-sidebar__heading">Master 1 IFRI {{date('Y')}}</li>
    </ul>
</div>
