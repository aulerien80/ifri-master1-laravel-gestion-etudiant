@if($errors->any())
    <div class="container">
        <div class="row mb-1">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="alert alert-dismissible alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endif
