@extends('layout.layout')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fa fa-user bg-mean-fruit">
                    </i>
                </div>
                <div>Etudiants
                    <div class="page-title-subheading">Liste des étudiants.
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layout.partials.success-error-message')

    <div class="row">
        <div class="col-lg-12 col-md-12 text-right mb-2">
            <a href="{{route('etudiant.creation')}}" class="btn btn-primary">
                <i class="fa fa-plus"></i> Ajouter un nouvel étudiant
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">@lang('message.liste_etudiants')</h5>
                    <table class="mb-0 table table-bordered">
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Prénom(s)</th>
                            <th>Sexe</th>
                            <th>Date Naissance</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($etudiants as $etudiant)
                            <tr>
                                <td>{{$etudiant->nom}}</td>
                                <td>{{$etudiant->prenom}}</td>
                                <td>{{$etudiant->sexe == 'F' ? 'Féminin' : 'Masculin'}}</td>
                                <td>{{\Illuminate\Support\Carbon::parse($etudiant->date_naissance)->format('d/m/Y')}}</td>
                                <td>{{$etudiant->email ?? '-'}}</td>
                                <td>
                                    <a href="{{route('etudiant.modification', ['etudiantId' => $etudiant->id])}}">
                                        <i title="Modifier" class="fa fa-pen text-primary"></i>
                                    </a>
                                    &nbsp;
                                    <a style="cursor: pointer" class="mr-2 mb-2">
                                        <i onclick="document.getElementById('deleteEtudiantForm{{$etudiant->id}}').submit()"
                                           class="fa fa-times text-danger"></i>
                                    </a>
                                    <form id="deleteEtudiantForm{{$etudiant->id}}"
                                          action="{{route('etudiant.delete', ['etudiantId' => $etudiant->id])}}"
                                          method="post">
                                        @csrf
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
