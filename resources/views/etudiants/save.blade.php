@extends('layout.layout')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fa fa-user bg-mean-fruit">
                    </i>
                </div>
                <div>Etudiants
                    <div class="page-title-subheading">@lang('message.liste_etudiants')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- message -->
    @include('layout.partials.success-error-message')

    <div class="row">
        <div class="col-lg-12 col-md-12 text-left mb-2">
            <a href="{{route('etudiant.liste')}}" class="btn btn-secondary">
                <i class="fa fa-backward"></i> @lang('message.retour_liste_etudiants')
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body"><h5 class="card-title">
                    @if(isset($etudiant))
                        Modification d'un étudiant
                        @else
                            Création d'un nouvel étudiant
                        @endif
                    </h5>
                    <form  action="{{route('etudiant.save')}}" method="post">
                        @csrf
                        @if(isset($etudiant))
                            <input type="hidden" name="etudiant_id" value="{{$etudiant->id}}">
                        @endif
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="nom">Nom</label>
                                <input type="text" class="form-control @if($errors->has('nom')) is-invalid @endif"
                                       id="nom" name="nom" placeholder="nom" value="{{old('nom', isset($etudiant) ? $etudiant->nom : '' )}}">
                                @if($errors->has('nom'))
                                <div class="invalid-feedback">
                                    {{$errors->first('nom')}}
                                </div>
                                @endif
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="prenom">Prénom(s)</label>
                                <input type="text" class="form-control @if($errors->has('prenom')) is-invalid @endif"
                                       id="prenom" name="prenom" placeholder="prenom" value="{{old('prenom', isset($etudiant) ? $etudiant->prenom : '' )}}">
                                @if($errors->has('prenom'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('prenom')}}
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="sexe">Sexe</label>
                                <select name="sexe" id="sexe"  class="form-control @if($errors->has('sexe')) is-invalid @endif">
                                    <option value="" disabled selected>Sélectionner le sexe</option>
                                    <option @if(isset($etudiant) && $etudiant->sexe == 'F') selected @endif value="F">Féminin</option>
                                    <option @if(isset($etudiant) && $etudiant->sexe == 'M') selected @endif value="M">Masculin</option>
                                </select>
                                @if($errors->has('sexe'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('sexe')}}
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="date_naissance">Date naissance</label>
                                <input id="date_naissance" type="date" max="{{date('Y-m-d')}}"
                                       name="date_naissance" class="form-control @if($errors->has('date_naissance')) is-invalid @endif"
                                value="{{old('date_naissance', isset($etudiant) ? $etudiant->date_naissance : '' )}}">
                                @if($errors->has('date_naissance'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('date_naissance')}}
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="email">Email</label>
                                <input id="email" name="email" type="email" class="form-control"
                                       value="{{old('email', isset($etudiant) ? $etudiant->email : '' )}}">
                            </div>

                        </div>
                        <div class="text-center">
                            <button class="btn btn-success" type="submit">Enregistrer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
