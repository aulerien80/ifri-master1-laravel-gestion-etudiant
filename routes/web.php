<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'HomeController@index')->name('accueil.default');

Route::get('/accueil', 'HomeController@accueil')->name('accueil.custom');

// ETUDIANTS
Route::group(['prefix' => 'etudiants'], function (){
    Route::get('liste', 'EtudiantController@list')->name('etudiant.liste');
    Route::get('modification/{etudiantId}', 'EtudiantController@modification')->name('etudiant.modification');
    Route::get('creation', 'EtudiantController@creation')->name('etudiant.creation');
    Route::post('save', 'EtudiantController@save')->name('etudiant.save');
    Route::post('suppression/{etudiantId}', 'EtudiantController@delete')->name('etudiant.delete');
});
